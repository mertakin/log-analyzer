﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;
using System.Windows.Forms;


namespace WpfLayout
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : System.Windows.Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        


        private void GetFilesFromFolderPath(string folderpath)
        {
            string[] files = Directory.GetFiles(folderpath, ("*.log"), System.IO.SearchOption.AllDirectories);
            List<Folder> foders = files.Select(x => new Folder(x)).ToList();
            foreach( var item in foders)
            {
               // item = ShowLogInformation(item.FolderName);
            }

            listBox1.ItemsSource = foders;
        }

        private void textBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)

        {
        //while (textBox.Text.Length > 0) ;
        if (e.Key == Key.Enter)
            {
                string path = textBox.Text;
                if (Directory.Exists(path))
                    {
                    GetFilesFromFolderPath(path);
                    }
            }
        }



   
        private void button_Click(object sender, RoutedEventArgs e)
        {


            FolderBrowserDialog FBD = new FolderBrowserDialog();
            if (FBD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                GetFilesFromFolderPath(FBD.SelectedPath);

                //string[] files = Directory.GetFiles(FBD.SelectedPath, ("*.log"), System.IO.SearchOption.AllDirectories);
                //listBox1.ItemsSource = null;
                //listBox1.ItemsSource = files;
            }


             //string[] lines = File.ReadAllLines(@"C:\Users\XXX\Desktop\context.log");
             //List<string> operations = new List<string>();
             //ShowLogInformation(@"path");
        }

       

       

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            try
            {
                ShowLogInformation((listBox1.SelectedItem as Folder).FolderName);

                textBox.Text = listBox1.SelectedValue.ToString();
            }
            catch (Exception ex)
            {

                Logger.LogException(ex);
            }

        }


        


        public Folder ShowLogInformation(string filePath)
        {
            Folder output = new Folder(filePath);
            string[] lines = File.ReadAllLines(filePath);

            string pattern = "OpName\\s=\\s" + "\"" + "([A-Za-z\\s]+)";

            string StartPattern = "TimeStamp\\s=\\s" + "\"" + "(\\d{4}-\\d{2}-\\d{2}" + "," + "\\s\\d{2}:\\d{2}:\\d{2}.\\d{4})";

            string EndPattern = "End\\" + "\"" + "\\s" + "TimeStamp\\s=\\s" + "\"" + "(\\d{4}-\\d{2}-\\d{2}" + "," + "\\s\\d{2}:\\d{2}:\\d{2})";

            string PeriodPattern = "Period\\s=\\s" + "\"" + "(\\d{2}:\\d{2}:\\d{2}" + "." + "\\d{7})";





            Regex regex = new Regex(pattern);

            Regex StartRegex = new Regex(StartPattern);

            Regex EndRegex = new Regex(EndPattern);

            Regex PeriodRegex = new Regex(PeriodPattern);





            List<OperationRecord> operations = new List<WpfLayout.OperationRecord>();



            foreach (var line in lines)
            {

                Match match = regex.Match(line);
                Match Startmatch = StartRegex.Match(line);
                Match Endmatch = EndRegex.Match(line);
                Match Periodmatch = PeriodRegex.Match(line);


                if (match.Success)
                {
                    if (!operations.Any(x => x.OperationName == match.Groups[1].Value))
                        operations.Add(new WpfLayout.OperationRecord(match.Groups[1].Value));
                }
                if (Startmatch.Success)
                {
                    DateTime start = Convert.ToDateTime(Startmatch.Groups[1].Value);
                    operations.First(x => x.OperationName == match.Groups[1].Value).AddStartTime(start);

                }
                if (Endmatch.Success)
                {
                    DateTime end = Convert.ToDateTime(Endmatch.Groups[1].Value);

                    operations.First(x => x.OperationName == match.Groups[1].Value).AddEndTime(end);
                }

                if (Periodmatch.Success)
                {

                    TimeSpan period = TimeSpan.Parse(Periodmatch.Groups[1].Value);
                    operations.First(x => x.OperationName == match.Groups[1].Value).AddPeriod(period);
                }

            }
            output.Operations = operations;
            output.IsCompleted = lines.Last().Contains("</Registery>");


            


            // check if there is any error 
            output.HasError = lines.Any(x => x.Contains("\"Exc\""));

            dataGrid.ItemsSource = operations;


            return output;

            
        }

        private void listBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }





}









