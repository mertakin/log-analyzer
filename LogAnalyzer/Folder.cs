﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WpfLayout;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Microsoft.Win32;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WpfLayout
{
    public class Folder
    {
        public string FolderName { get; set; }

        public bool HasError { get; set; }

        public bool IsCompleted { get; set; }


        public List<OperationRecord> Operations { get; set; }


        public Folder(string path)
        {
            FolderName = path;
            HasError = false;
            IsCompleted = true;
            Operations = new List<WpfLayout.OperationRecord>();
                


    }
    }
}