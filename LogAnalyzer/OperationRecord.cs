﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfLayout
{
    public class OperationRecord
    {
        public string OperationName { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public TimeSpan Period { get; set; }


        public OperationRecord(string OperationName, DateTime Start, DateTime End, TimeSpan Period)
        {

        }


        public OperationRecord(string operationName)
        {
            this.OperationName = operationName;
        }


        public void AddStartTime(DateTime start)
        {
            this.Start = start;
        }


        public void AddEndTime(DateTime end)
        {
            this.End = end;
        }


        public void AddPeriod(TimeSpan period)
        {
            this.Period = period;
        }



    }
}
