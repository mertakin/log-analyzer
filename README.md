# Log Analyzer

### Summary

Log analyzer is a WPF application which helps to analyze log files using regular expressions.

### Demonstration

![](loganalyzer.gif)